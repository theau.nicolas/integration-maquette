class Card extends HTMLElement {

    static get observedAttributes() { return ['name', 'url'] }

    constructor() {
        super();
        this.name;
        this.url;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log("cs")
        switch (true) {
            case name == "name" && oldValue !== newValue:
                this.name = newValue
                break;

            case name == "url" && oldValue !== newValue:
                this.url = newValue
                break;

            default:
                break;
        }
    }

    // connect component
    connectedCallback() {
        this.innerHTML =
            `<div class='card flex-row'>
                <img src="${this.url}" alt="coucou"/>
                <section>
                    <h1>${this.name}</h1>
                    <button>Place a bid</button>
                </section>
            </div>`
    }

}

customElements.define('custom-card', Card)