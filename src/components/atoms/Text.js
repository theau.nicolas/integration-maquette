class CustomText extends WishComponent {
    static get template() {
        return `
        <style>
            .wish-text {
                font-weight: "light",
                font-size: "10px"
            }
        </style>
            <p class="wish-text">
                <slot></slot>
            </p>
        `;
    }
}
customElements.define('wish-text', CustomText);