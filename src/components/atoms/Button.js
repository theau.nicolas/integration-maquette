class CustomButton extends WishComponent {
    constructor() {
        super()
        this.template = `
        <style>
            .custom-button {
                background-color : {color};
                border : none;
                padding: 5px;
                border-radius: 5px;
            }
        </style>
        <button class="custom-button" >
            <slot></slot>
        </button>
        `;
    }
}
customElements.define('wish-button', CustomButton);