class BidCard extends HTMLElement {

    static get observedAttributes() { return ['name', 'url'] }

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.innerHTML =
            `<div class='card flex-row'>
                <img src="${this.url}" alt="coucou"/>
                <section>
                    <h1>${this.name}</h1>
                    <button>Place a bid</button>
                </section>
            </div>`;
        this.contentElement = this.shadowRoot.querySelector('.custom-element__content');

    }

    attributeChangedCallback(attribute, oldValue, newValue) {
        if (oldValue !== newValue) {
            this[attribute] = newValue
        }
    }

    set content(content) {
        this.contentElement.innerHTML = content;
    }
    get content() {
        return this.contentElement.innerHTML;
    }

    // connect component
    // connectedCallback() {
    //     this.innerHTML =
    //         `<div class='card flex-row'>
    //             <img src="${this.url}" alt="coucou"/>
    //             <section>
    //                 <h1>${this.name}</h1>
    //                 <button>Place a bid</button>
    //             </section>
    //         </div>`
    // }
}

customElements.define('bid-card', BidCard)
