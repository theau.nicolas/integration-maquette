class WishComponent extends HTMLElement {
    constructor() {
        super();
        this._template = null;
        this.props = {};
        this.updateProps();
        this.attachShadow({ mode: 'open' });
    }

    connectedCallback() {
        this.updateProps();
        this.shadowRoot.innerHTML = this.template;
    }

    updateProps() {
        for (let i = 0; i < this.attributes.length; i += 1) {
            const attribute = this.attributes[i];
            this.props[attribute.name] = attribute.value;
        }
    }

    get template() {
        let template = this._template;

        for (const key in this.props) {
            const re = new RegExp(`\{${key}\}`, "g");
            template = template.replace(re, this.props[key]);
        }
        return template;
    }

    set template(value) {
        this._template = value;
    }

}